import React from "react"
import { Button } from '@material-ui/core';

import {
    CIcon
  } from '@coreui/icons-react';

const Export = ({ onExport }) => {
  return (
    <Button
      title="Exportar"
      color="light"
      onClick={e => onExport(e.target.value)}
    >
     <CIcon name="cil-file-xls" size="2xl" />
    </Button>
  )
}

export default Export
