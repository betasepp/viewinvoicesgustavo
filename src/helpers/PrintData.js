import React from "react";
import jsPDF from "jspdf";
import "jspdf-autotable";
import { format } from "date-fns";
import { CSVLink } from "react-csv";
import {
  CIcon
} from '@coreui/icons-react';

export const PrintDataCSV = props => {
  const camelCase = str => {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
  };

  const filterColumns = data => {
    // Get column names
    if(data[0]!=null && data[0]!=undefined) {
      const columns = Object.keys(data[0]);
      let headers = [];
      columns.forEach((col, idx) => {
        if (idx !== 0) {
          headers.push({ label: camelCase(col), key: col });
        }
      });
  
      return headers;
    }
    
  };

  const date = Date().split(" ");
  // we use a date string to generate our filename.
  const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];

  const title = props.title || '';

  return (
    <CSVLink 
        data={props.data} 
        separator={'\t'}
        headers={filterColumns(props.data)} 
        filename={`AppFiorito_${title}_${dateStr}.csv`}>
        <CIcon name="cil-file-xls" size="2xl" />{props.title && <><br/>{props.title}</>}
    </CSVLink>
  )
}
const getColumnNames = (data) => {
  let result = [];
  if (data === null || data === undefined) {
      return result;
  }
  if (data.length > 0) {
      Object.keys(data[0]).map((key) => {
          result.push(key);
      });
  }
  return result;
}

export const printOperacionesData = (headerText, data) => {
  const doc = new jsPDF();

  const tableColumn = getColumnNames(data); //["Id", "Cuit", "Razón Social", "Tipo", "Subtipo", "Fecha Alta", "Estado"];

  const tableRows = [];

  data.forEach(item => {
    let itemData = [];
    tableColumn.map(col => {
      itemData.push(item[col]);
    });
    // const itemData = [
    //   item.id,
    //   item.cuit,
    //   item.razonSocial,
    //   item.tipodescripcion,
    //   item.subtipodescripcion,
    //   format(new Date(item.fechaAlta), "yyyy-MM-dd"),
    //   item.estadodescripcion
    // ];
    tableRows.push(itemData);
  });

  // ticket title. and margin-top + margin-left
  doc.text(headerText, 14, 15);
  // startY is basically margin-top
  doc.autoTable(tableColumn, tableRows, { startY: 30,
    didParseCell: function (data) {
      if (data.section === "head") {
        data.cell.styles.textColor = 255;
        data.cell.styles.fillColor = [202, 106, 104];
        data.cell.styles.fontStyle = 'bold';
      }
    } });
  const date = Date().split(" ");
  // we use a date string to generate our filename.
  const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];
  // we define the name of our PDF file.
  doc.save(`fiorito_${dateStr}.pdf`);
};

export const printMiscelaneosData = data => {
    const doc = new jsPDF();
  
    const tableColumn = ["Id", "Cuit", "Razón Social", "Tipo", "Fecha Alta", "Estado"];
  
    const tableRows = [];
  
    data.forEach(item => {
      const itemData = [
        item.id,
        item.cuit,
        item.razonSocial,
        item.tipodescripcion,
        format(new Date(item.fechaAlta), "yyyy-MM-dd"),
        item.estadodescripcion
      ];
      tableRows.push(itemData);
    });
  
    // startY is basically margin-top
    doc.autoTable(tableColumn, tableRows, { startY: 20 });
    const date = Date().split(" ");
    // we use a date string to generate our filename.
    const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];
    // ticket title. and margin-top + margin-left
    doc.text("MISCELANEOS", 14, 15);
    // we define the name of our PDF file.
    doc.save(`miscelaneos_${dateStr}.pdf`);
  };