import Axios from 'axios';
import { alertService } from '@services/alert-service';
// import {getToken} from './auth-helpers';

const STATUS_OK = 200;

const {
    API_FIORITO_URL: host,
    API_FIORITO_VERSION: version,
} = window;

async function isLoggerInDatabase() {
    let result = "";
    await Axios.get(host + version + "/configuracion/findByCodigo/LOGGER").then(response => {
        if (response.status === STATUS_OK) {
            result = response.data.valor;
        } else {
            console.log("isLoggerInDatabase status --> ", response.status, " - ", response.statusText);
        }
    }).catch(error => {
        console.log("isLoggerInDatabase error --> ", error);
    });
    return result;
}

export function consoleLog(action, ...args) {
    // const {username} = JSON.parse(getToken());
    let strError = args.toString();
    let logTipo = "W";
    
    if (action.toString().toLowerCase().includes('error')) {
        let autoClose = true;
        let keepAfterRouteChange = false;
        alertService.error(args.toString(), { autoClose, keepAfterRouteChange });
    }

    if (action.toString().toLowerCase().includes('axios error') && args[0].response) {
        let autoClose = true;
        let keepAfterRouteChange = false;
        alertService.error(args[0].response.data.trace, { autoClose, keepAfterRouteChange });
        strError = args[0].response.data.trace;
    }

    // isLoggerInDatabase().then(response => {
    //     if (response !== "S") return;

    //     let mensaje = "";
    //     if (action.includes('error')) {
    //         mensaje = JSON.stringify({ error: strError });
    //         logTipo = "E";
    //     } else {
    //         mensaje = strError;
    //     }
    //     // } else {
    //     //     let msg = {};
    //     //     args.map(item => {
    //     //         msg = {
    //     //             ...msg,
    //     //             item
    //     //         };
    //     //     });
    //     //     mensaje = JSON.stringify(msg);
    //     // }

    //         let log = {
    //             tipo: logTipo,
    //             accion: action,
    //             //fecha: new Date(),
    //             mensaje: mensaje,
    //             usuario: ""
    //         };

    //         Axios.post( host + version + "/auditLogs", log).then(response => {
    //             if (response.status !== STATUS_OK) {
    //                 console.log("consoleLog status --> ", response.status, " - ", response.statusText);
    //             }
    //         }).catch(error => {
    //             console.log("consoleLog error --> ", error);
    //         });
    //     }

    // );

}


