import  {React, useState, useEffect, useMemo ,createContext,useContext} from 'react';
import { useAsyncError } from "../helpers/AsyncErrorHandler";
import mqtt from 'mqtt';
import encoding from 'text-encoding';
import {useAuth} from './AuthContext';

const MqttContext = createContext();

export function MqttProvider(props) {
    const {user} = useAuth();
    const [client, setClient] = useState(null);
    const [isConnected, setIsConnected] = useState(false);
    

    useEffect(() => {
        if (user) {
            const ws = mqtt.connect(process.env.REACT_APP_MQTT_URL);
            ws.stream.on('connect', () => {
                setIsConnected(true);
                console.log('mqttClient connected');
            });
            ws.stream.on('error', (err) => {
                setIsConnected(false);
                console.log(err);
            });
            setClient(ws);
        }
    }, [user]);

    function subscribe(client, topic, errorHandler) {
        const callBack = (err, granted) => {
            if (err) {
            errorHandler('Subscription request failed');
            }
        };
        return client.subscribe(process.env.REACT_APP_MQTT_API_ENDPOINT + topic, callBack);
    }

    function onMessage(client, callBack) {
        client.on('message', (topic, message, packet) => {
            callBack(JSON.parse(new encoding.TextDecoder('utf-8').decode(message)));
        });
    }

    function unsubscribe(client, topic) {
        client.unsubscribe(process.env.REACT_APP_MQTT_API_ENDPOINT + topic);
    }
    
    function closeConnection(client) {
        client.end();
    }
      
    const value = useMemo(() => {
    return ({
        client,
        isConnected,
        subscribe,
        onMessage,
        unsubscribe,
        closeConnection
    })
    }, [client]);

    return <MqttContext.Provider value={value} {...props} />
}

export function useMqtt() {
    const context = useContext(MqttContext);
    const throwError = useAsyncError();
    if (!context) {
        throwError(new Error('useMqtt must be in MqttContext Provider'));
    }
    return context;
}