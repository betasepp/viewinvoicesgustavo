import { React,useState, useEffect, useMemo,createContext,useContext} from 'react';
import i18n from '../i18n';

const LanguageContext = createContext();

export function LangProvider(props) {

    const [language, setLanguage] = useState('');
    const [changingLanguage, setChangingLanguage] = useState(false);
 
    useEffect(() => {
        async function languageLoad() {
          setLanguage(i18n.language);
          if(localStorage.getItem('changingLanguage')) {
            setChangingLanguage(true);
          }
        }
        languageLoad();
      }, []);

      function changeLanguage(isChanging){
        setChangingLanguage(isChanging);
        if (isChanging){
          localStorage.setItem('changingLanguage','true');
          setLanguage(i18n.language);
        } else {
          localStorage.removeItem('changingLanguage');
        }
      }

      const value = useMemo(() => {
          return ({
            language,
            changingLanguage,
            changeLanguage
          })
      }, [language, changingLanguage])

      return <LanguageContext.Provider value={value} {...props} />
}

export function useLang() {
    const context = useContext(LanguageContext);
    if (!context) {
        throw new Error('useLang must be in LanguageContext Provider');
    }
    return context;
}