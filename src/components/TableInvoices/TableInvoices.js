import {
    cilChevronCircleDownAlt, cilCloud, cilCloudDownload,
    cilCloudUpload
} from "@coreui/icons"
import CIcon from '@coreui/icons-react'
import {
    CButton, CInputGroup, CTable,CInputGroupText,CFormInput
} from "@coreui/react"
import { useEffect, useState } from "react"
import { useNotification } from "src/notification/NotificationService"
import { uploadInvoice } from "src/services/invoicesServices"
import { printerInvoiceDownload } from "../../services/printerServices"
const FileDownload = require('js-file-download');

  const TableInvoices = ({data}) => {
    const [items ,setItems] = useState([])
    const [selectedFiles,setSelectedFiles] = useState(false)
    const  setNotification  = useNotification()

    const columns = [
        { key: '1', label: 'Tipo', _props: { scope: 'col' } },
        { key: '2', label: 'Punto', _props: { scope: 'col' } },
        { key: '3', label: 'Numero', _props: { scope: 'col' } },
        { key: '4', label: 'Fecha', _props: { scope: 'col' } },
        { key: '5', label: 'Importe', _props: { scope: 'col' } },
        { key: '6', label: 'Saldo', _props: { scope: 'col' } },
        { key: '7', label: '', _props: { scope: 'col' } },
        { key: '8', label: '', _props: { scope: 'col' } },
        { key: '9', label: '', _props: { scope: 'col' } },
    ]

    useEffect(() => {
        if(data) {
            addData()
        }
    },[data])  //eslint-disable-line

    const handlePrinterDownload =(id,name)=>{
        printerInvoiceDownload(id).then(response => {
            if(response) {
                FileDownload(response,name)
            }else {console.log("not response")}
          }).catch(error => {
            console.log(error)
          }).finally(() => {
          })
    }

    function selectFile (event,code) {
        let file = event.target.files[0];
        handlePrinterUpload(code,file)
    }

    const handlePrinterUpload =(code,file)=>{
        uploadInvoice(code,file).then(response =>{
            setNotification('success',file.name + " subido correctamente.", 5)
        }).catch(error =>{
            setNotification('danger',file.name + " no se pudo subir." + error, 5)    
        }).finally(()=>{
            setNotification('success',file.name + " ya disponible.", 5)
        })
    }


    const handlePrinterNotDownload =(name)=>{
        setNotification('danger',name + " no disponible. Solicite que suban su comprobante.", 5)
    }

    const addData =() =>{
        const response=   data.map(invoice => {
            return {
                1: invoice.invoiceType,
                2: invoice.invoicePointOfSale,
                3: invoice.invoiceNumber,
                4: invoice.invoiceDatePatternESP,
                5: invoice.invoiceTotal,
                6: (invoice.invoicePaid?"0":invoice.invoiceBalance),
                7: <CButton color="primary" href={invoice.afipCheck} className="float-end" ><CIcon icon={cilChevronCircleDownAlt} />Afip</CButton>,
                8: invoice.file? 
                    <CButton color="primary" href={invoice.printerPath}  className="float-end" ><CIcon icon={cilCloudDownload} /> </CButton>
                    :
                    <CButton color="" onClick={() => handlePrinterNotDownload(invoice.fileName)} className="float-end" ><CIcon icon={cilCloud}/> </CButton>
                    ,
                _cellProps: { class: { scope: 'row' } },                
                9: 
                    (selectedFiles && <CInputGroup className="mb-8">
                        <CInputGroupText component="label" htmlFor="inputGroupFile01"></CInputGroupText>
                        <CFormInput type="file" id="inputGroupFile01" onChange={() => selectFile(event,invoice.code)}/>
                    </CInputGroup>)
                    ,
            }
        })
        setItems(response)
    }

    return <CTable columns={columns} items={items} tableHeadProps={{ color: 'dark' }}/>
  }

  export default TableInvoices
