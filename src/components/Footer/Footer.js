import React, { useState ,useEffect} from 'react'
import { CFooter } from '@coreui/react'
import { versionFront,versionBack } from 'src/services/systemServices'
//import metadata from '../../metadata.json'

const Footer = () => {
  const [backendVersion,setBackendVersion] = useState('')
  const {version} = versionFront

  useEffect(() => {
    componentDidMount()
  },[])

  const componentDidMount = () => {
    versionBack().then(response => {
      setBackendVersion(response)
    });
  }

  return (
    <CFooter>
      <div>
        <a href="https://github.com/andresGitDev" target="_blank" rel="noopener noreferrer">Developer </a>
        <span className="me-1">App Version: {version} {`, Back Version: ${backendVersion}`}</span>
      </div>
      <div className="ms-auto">
        <span className="copyright text-center my-auto me-1">Powered by</span>
        <a href="http://www.betasepp.com.ar/" target="_blank" rel="noopener noreferrer">
          <span>Copyright &copy; Betasepp 2023</span>
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(Footer)



