import React ,{useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { Link } from 'react-router-dom'
import Loading from 'src/components/Loading/Loading'
import { useNotification } from '../../../notification/NotificationService'
import { useAuth } from 'src/context/AuthContext'

const Register = () => {
  const [cuit, setCuit] = useState('')
  const [mail, setMail] = useState('')
  const [loading, setLoading] = useState(false)
  const  setNotification  = useNotification()
  const  {sendMailClient,getClientDataDB}  = useAuth()

  const requestPassword =  () => {
    if(!mail) {setNotification('danger',"Debe ingresar mail.", 5)}
    if(!cuit) {setNotification('danger',"Debe ingresar cuit.", 5)}

    if(cuit && mail) {
      setLoading(true)
      getClientDataDB(cuit).then(result => {
        if(result.message==="") {
          sendMailClient(cuit,mail).then(response => {
            setNotification('success',response, 5)
          }).catch(error => {
            setNotification('danger',error, 5)
          }).finally(() => {
            setLoading(false)
          })              
        } else{
          setNotification('danger',result.message, 5)
        }
      }).catch(error => {
        setNotification('danger',error, 5)
      }).finally(() => {
        setLoading(false)
      })   
    }
}

if(loading) {
  return (<Loading message="Enviando"></Loading>)
}

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
    <CContainer>
      <CRow className="justify-content-center">
        <CCol md={9} lg={7} xl={6}>
          <CCard className="mx-4">
            <CCardBody className="p-4">
              <CForm>
                <h1>Register</h1>
                <p className="text-medium-emphasis">Solicitud de clave</p>
                <CInputGroup className="mb-3">
                  <CInputGroupText>
                    <CIcon icon={cilUser} />
                  </CInputGroupText>
                  <CFormInput required placeholder="Cuit" autoComplete="Cuit" value={cuit} onChange={(event) => setCuit(event.target.value)}/>
                </CInputGroup>
                <CInputGroup className="mb-3">
                  <CInputGroupText>@</CInputGroupText>
                  <CFormInput required placeholder="Su correo" autoComplete="mail" value={mail} onChange={(event) => setMail(event.target.value)} />
                </CInputGroup>
                <div className="d-grid">
                  <CButton color="success" onClick={requestPassword}>Enviar</CButton>
                </div>
                <Link to="/login">
                    Ya tengo mi password
                </Link>                    
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </CContainer>
  </div>        
)
}

export default Register
