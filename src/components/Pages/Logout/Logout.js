import { React ,useEffect} from 'react' 
import { useAuth } from 'src/context/AuthContext'
import { useNotification } from '../../../notification/NotificationService'
import Loading from 'src/components/Loading/Loading'
import { useNavigate } from 'react-router-dom'

export const Logout = () => {
    const { deleteTokenData} = useAuth()
    const  setNotification  = useNotification()
    const navigate = useNavigate()

    useEffect(() => {
        deleteTokenData();
        setNotification('success',"Sesion cerrada", 5)
        navigate('/login')
    }, [])  //eslint-disable-line

    
    return (<Loading message="Cerrando session"></Loading>)

}

