import React ,{useState} from 'react'
import { Link } from 'react-router-dom'
import {CButton,CCard,CCardBody,CCardGroup,CCol,CContainer,CForm,CFormInput,CInputGroup,CInputGroupText,CRow,} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
//import logo from '../../../assets/img/brand/fiorito-logo.png'
import { useAuth } from 'src/context/AuthContext'
import metadata from "../../../../src/metadata.json"
import { Loguing } from 'src/services/loginServices'
import { useNotification } from '../../../notification/NotificationService'
import Loading from 'src/components/Loading/Loading'
import { InfoAppWeb ,InfoAppWebFront} from '../System/System'

const Login = () => {
  const [loading, setLoading] = useState(false)
  const [cuit, setCuit] = useState('')
  const [password, setPassword] = useState('')
  const [loginErrors, setLoginErrors] = useState(null)
  const { loadingUser, groups, handleSuccessfulAuth, logout,company,addSession ,getClient } = useAuth()
  const  setNotification  = useNotification()

  const handleSubmit = () => {
    setLoginErrors(null)
    if(!password) {setNotification('danger',"Debe ingresar password.", 5)}
    if(!cuit) {setNotification('danger',"Debe ingresar cuit.", 5)}

    if(cuit && password) {
        setLoading(true)
        getClient(cuit,password).then(response => {
            if(response.auth) {
                addSession(response);
                handleSuccessfulAuth(response.data, response.headers.authorization)
            } else {
                setNotification('danger',response.message, 5)
                setLoginErrors(response.status + " - " + response.statusText)
            }
        }).catch(error => {
            setNotification('danger',error, 5)
            setLoginErrors(error.toString().replace("Error:", "").replace("Error :", ""))
        }).finally(() => {
            setLoading(false)
        })
    }
  }

  
  const messageLogin=company?"Si es cliente de " + company.name + " solicite su clave a administracion para poder ingresar.":"Disculpe el servicio no esta en linea.";
    
  if(loading) {
      return (<Loading message="Iniciando"></Loading>)
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center"
      //style={{
      //  backgroundImage: `url(${logo})`,backgroundRepeat: "no-repeat",backgroundSize: "200px 200px", backgroundAttachment: "fixed", backgroundPosition: "top center",
      //}}
      >  
        
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-medium-emphasis">Inicie sesion con su cuenta de cliente</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput placeholder="Cuit" autoComplete="Cuit"  onChange={event => setCuit(event.target.value)} value={cuit} required/>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput type="password" placeholder="Clave" autoComplete="current-password" onChange={event => setPassword(event.target.value)} value={password} required/>
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="primary" className="btn btn-success btn-ladda" onClick={handleSubmit}>
                          Ingresar
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        <Link to="/register">
                            <CButton color="link" className="px-0">
                                Recuperar password?
                            </CButton>
                        </Link>
                      </CCol> 
                    </CRow>
                    <CRow>
                        <CCol xs="12" className="text-left">
                          {loginErrors && <><small style={{ color: 'red' }}>{loginErrors}</small></>}
                        </CCol>
                      </CRow>                    
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Registrarse</h2>
                    <p>
                        {messageLogin}
                    </p>                    
                    <Link to="/register">
                      <CButton color="primary" className="btn btn-success btn-ladda" active tabIndex={-1} data-color="link">
                        Solicitar password de acceso!
                      </CButton>
                    </Link>
                  </div>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                </CCardBody>
                <CCardBody className="text-center">
                  {company?<InfoAppWeb/>:<InfoAppWebFront/>}
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login