import React from 'react'
import ContentInvoices from 'src/components/Content/ContentInvoices'
import Sidebar from 'src/components/Sidebar/Sidebar'
import Footer from 'src/components/Footer/Footer'
import Header from 'src/components/Header/Header'

const Pendent = () => {
  return (
    <div>
      <Sidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <Header />
        <div className="body flex-grow-1 px-3">
          <ContentInvoices view="pendent" /> 
        </div>
        <Footer />
      </div>
    </div>
  )
}

export default Pendent
