import React from 'react'
import { useAuth } from 'src/context/AuthContext'
import {
  CAvatar,
  CDropdown,
  //CDropdownDivider,
  //CDropdownHeader,
  CBadge,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilLockLocked,
  cilSettings,
  cilUser,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'

import avatar0 from '../../assets/img/avatars/0.jpg'

const HeaderDropdown = () => {
  const { userName, logout,session } = useAuth()
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
          {session.name}  [{session.cuit}] 
          <CAvatar src={avatar0} size="md" />
      </CDropdownToggle>

      <CDropdownMenu className="pt-0" placement="bottom-end">
        {/* <CDropdownHeader className="bg-light fw-semibold py-2">Cuenta</CDropdownHeader> */}
        <CDropdownItem>
          <CIcon icon={cilUser} className="me-2" />
          Numero de cliente : 
          <CBadge color="primary" className="ms-2">
            {session.code}
          </CBadge>
        </CDropdownItem> 
        {/* <CDropdownDivider /> */}
        <CDropdownItem href="#" onClick={logout}>
          <CIcon icon={cilLockLocked} className="me-2" />
          Salir
        </CDropdownItem>        
      </CDropdownMenu>
    </CDropdown>
  )
}

export default HeaderDropdown
