import React, { useState,useEffect } from 'react'
import { 
  CContainer ,
} from '@coreui/react'
import Loading from '../Loading/Loading'
import TableInvoices from '../TableInvoices/TableInvoices'
import FindDate from '../FindDate/FindDate'
import { useAuth } from 'src/context/AuthContext'
import { useNotification } from '../../notification/NotificationService'
import {formatDate} from '../Pages/System/System'


const ContentInvoices = ({view}) => {
  const moonLanding = new Date();
  const [loading,setLoading] = useState(false)
  const [title ,setTitle] = useState('')
  const [onlyPendent ,setOnlyPendent] = useState("true")
  const { getInvoices,session} = useAuth()
  const  setNotification  = useNotification()
  const [invoices,setInvoices] = useState(null)
  //const [from,setFrom ] = useState((moonLanding.getFullYear()-1 ) + '-01-01')
  //const [to ,setTo] = useState(moonLanding.getFullYear() + '-01-01')
  const [from,setFrom ] = useState((moonLanding.getFullYear() ) + '-01-01')
  const [to ,setTo] = useState(formatDate(moonLanding,"yyyy-mm-dd"))

  useEffect(() => {
    setTitle(view? ( view==="invoices"?"Todas las Facturas":"Solo pendientes"  )  :"Seleccione una opcion" )
    setOnlyPendent(view? ( view==="invoices"?"false":"true"  )  :"true" )
    findInvoices(from,to)
  }, [view,onlyPendent]) //eslint-disable-line
  

  const findInvoices = (from,to) =>{
    setLoading(true)
    getInvoices(session.cuit,from,to,onlyPendent).then(response => {
      if(response) {
        setInvoices(response)
      }else {setNotification('danger','No se encontraron facturas.', 5)}
    }).catch(error => {
        setNotification('danger',error, 5)
    }).finally(() => {
      setFrom(from)
      setTo(to)
      setLoading(false)
    })
  }

  if(loading) {
    return <Loading message={title}></Loading>
  }

  return (
    <>
      <FindDate from={from} to={to} title={title} find={findInvoices} onlyPendent={onlyPendent}/>
      <CContainer lg>
        <TableInvoices data={invoices} />
      </CContainer>
    </>
  )
}

export default ContentInvoices
