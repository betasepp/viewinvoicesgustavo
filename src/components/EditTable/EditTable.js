import { CContainer} from '@coreui/react'
import React, { useCallback, useState, useMemo,useEffect} from 'react';
import MaterialReactTable from 'material-react-table';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Stack,
  TextField,
  Tooltip,
  //TableHead,
} from '@mui/material';
import { CButton } from '@coreui/react';
import { 
  Delete,
  //DeleteForeverOutlined, 
  Edit ,
  //Check,
  //Close
} from '@mui/icons-material';
import { MRT_Localization_ES } from './Localization';

const EditTable = ({title,type,columns,data,options,onSave}) => {
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [tableData, setTableData] = useState(() => data);
  const [validationErrors, setValidationErrors] = useState({});

  const handleCreateNewRow = (values) => {
    tableData.push(values);
    setTableData([...tableData]);
  };

  const handleSaveRowEdits = async ({ exitEditingMode, row, values }) => {
    if (!Object.keys(validationErrors).length) {
      Object.assign(tableData[row.index], values);//merge data in table
      let newData=JSON.parse(JSON.stringify(tableData[row.index]));
      onSave(newData)
      setTableData([...tableData]);
      exitEditingMode(); //required to exit editing mode and close modal
    }
  };

  const handleEditinRow = ({table,row}) => {
    table.setEditingRow(row)
  };

  const handleCancelRowEdits = ({table}) => {
    setValidationErrors({});
  };

  const handleDeleteRow = useCallback(
    (row) => {
      if (
        !confirm(`Are you sure you want to delete ${row.getValue('firstName')}`)
      ) {
        return;
      }
      //send api delete request here, then refetch or update local table data for re-render
      tableData.splice(row.index, 1);
      setTableData([...tableData]);
    },
    [tableData],
  );

  const getCommonEditTextFieldProps = useCallback(
    (cell) => {
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid =
            cell.column.id === 'email'
              ? validateEmail(event.target.value)
              : cell.column.id === 'age'
              ? validateAge(+event.target.value)
              : validateRequired(event.target.value);
          if (!isValid) {
            //set validation error for cell if invalid
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            //remove validation error for cell if valid
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors],
  );

  const columns2 = useMemo(
    () => [
      {
        accessorKey: 'id',
        header: 'ID',
        enableColumnOrdering: false,
        enableEditing: false, //disable editing on this column
        enableSorting: false,
        size: 80,
      },
      {
        accessorKey: 'firstName',
        header: 'First Name',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'lastName',
        header: 'Last Name',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'email',
        header: 'Email',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
          type: 'email',
        }),
      },
      {
        accessorKey: 'age',
        header: 'Age',
        size: 80,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
          type: 'number',
        }),
      },
      {
        accessorKey: 'state',
        header: 'State',
        muiTableBodyCellEditTextFieldProps: {
          select: true, //change to select for a dropdown
          // children: states.map((state) => (
          //   <MenuItem key={state} value={state}>
          //     {state}
          //   </MenuItem>
          // )),
        },
      },
    ],
    [getCommonEditTextFieldProps],
  );

  return (
    <>
      <h1></h1>
      <MaterialReactTable
        initialState={{ pagination: { pageSize: 5, pageIndex: 0 }}}//{{ pagination: { pageSize: 5, pageIndex: 0 },columnVisibility: { id: false }  }}
        //muiTableContainerProps={{ sx: { maxHeight: '600px' } }}
        //muiTableBodyCellEditTextFieldProps={{ variant: 'outlined' }}
        localization={MRT_Localization_ES}
        columns={columns}
        data={tableData}
        enableEditing={options.edition}
        displayColumnDefOptions={{'mrt-row-actions': { muiTableHeadCellProps: {sx: { background: '#CA6A68',color: '#FFF',fontSize: '11px' }, align: 'center',},size: 120,},}}
        editingMode={options.modeEdition}
        enableColumnActions={options.columnAction}
        enableColumnOrdering={options.columnOrdering}
        enableGlobalFilter={options.search}
        enableHiding={options.columnHiding}
        enableColumnFilters={options.columnFilters}
        enableFullScreenToggle={options.fullScreen}
        enableDensityToggle={options.density}
        onEditingRowSave={handleSaveRowEdits}
        onEditingRowCancel={handleCancelRowEdits}
        positionActionsColumn={options.positionActions}//'last'
        enableRowActionsn={options.actions}
        renderRowActions= {options.actions?  ({ row, table }) => (
            <Box sx={{ display: 'flex', gap: '1rem' }}>
              <Tooltip arrow placement="left" title="Editar">
                <IconButton onClick={() => handleEditinRow({table,row})}>
                  <Edit />
                </IconButton>
              </Tooltip>
              <Tooltip arrow placement="right" title="Borrar">
                <IconButton onClick={() => handleDeleteRow(row)}><Delete /></IconButton>
              </Tooltip>
            </Box>
        ):null}
        
        renderTopToolbarCustomActions={() => (
          <CContainer>
            <h4>{title}</h4>
          </CContainer>
        )}
        renderToolbarInternalActions={() => (
          options.buttonNewActive &&
          <CButton color='primary' onClick={() => setCreateModalOpen(true)} >
            Nuevo {type}
          </CButton>
        )}        
      />
      <CreateNewAccountModal
        columns={columns}
        open={createModalOpen}
        onClose={() => setCreateModalOpen(false)}
        onSubmit={handleCreateNewRow}
      />
    </>
  );
};

//example of creating a mui dialog modal for creating new rows
export const CreateNewAccountModal = ({ open, columns, onClose, onSubmit }) => {
  const [values, setValues] = useState(() =>
    columns.reduce((acc, column) => {
      acc[column.accessorKey ?? ''] = '';
      return acc;
    }, {}),
  );

  const handleSubmit = () => {
    //put your validation logic here
    onSubmit(values);
    onClose();
  };

  return (
    <Dialog open={open}>
      <DialogTitle textAlign="center">Nuevo</DialogTitle>
      <DialogContent>
        <form onSubmit={(e) => e.preventDefault()}>
          <Stack
            sx={{
              width: '100%',
              minWidth: { xs: '300px', sm: '360px', md: '400px' },
              gap: '1.5rem',
            }}
          >
            {columns.map((column) => (
              <TextField
                key={column.accessorKey}
                label={column.header}
                name={column.accessorKey}
                onChange={(e) =>
                  setValues({ ...values, [e.target.name]: e.target.value })
                }
              />
            ))}
          </Stack>
        </form>
      </DialogContent>
      <DialogActions sx={{ p: '1.25rem' }}>
        <CButton onClick={onClose}>Cancelar</CButton>
        <CButton color="primary" onClick={handleSubmit} >
          Guardar
        </CButton>
      </DialogActions>
    </Dialog>
  );
};

const validateRequired = (value) => !!value.length;
const validateEmail = (email) =>
  !!email.length &&
  email
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
const validateAge = (age) => age >= 18 && age <= 50;

export default React.memo(EditTable)

