import React, { Component, useState, useEffect, forwardRef } from "react";
import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class EditTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      columns: props.columns,
      options: props.options,
      resolve: () => { },
      reject: () => { },
      updatedAt: new Date(),
      title: props.title,
      deletable: props.deletable,
      editable: props.editable,
      insertable: props.insertable
    };
  }

  componentWillReceiveProps = nextProps => {
    // console.log('componentWillReceiveProps --> ', nextProps);
    this.setState({
      data: nextProps.data,
      columns: nextProps.columns,
      options: nextProps.options
    });
  };

  componentDidMount() {
    this.state.resolve();
    // console.log("RESOLVE AT:", this.state.updatedAt, this.state.data);
  }

  onUpdate = async (newData, oldData) => {
    const { onUpdate } = this.props;
    if (onUpdate !== undefined) {
      let result = await onUpdate(newData, oldData);
      if (!result) {
        const data = [...this.state.data];
        const index = data.indexOf(newData);
        data[index] = oldData;
        this.setState({ ...this.state, data });
        this.state.reject();
        return false;
      }
    }
    this.state.resolve();
  }

  onAdd = async (newData) => {
    const { onAdd } = this.props;
    if (onAdd !== undefined) {
      let result = await onAdd(newData);
      if (!result) {
        var index = this.state.data.indexOf(newData)
        if (index !== -1) {
            this.state.data.splice(index, 1);
        }
        this.state.reject();
        return false;
      }
    }
    this.state.resolve();
  }

  onRowAdd = newData => {
    if (!this.state.insertable) {
      return false;
    }
    new Promise((resolve, reject) => {
      const data = [...this.state.data];
      data.push(newData);
      const updatedAt = new Date();
      this.setState({ ...this.state, data, updatedAt, resolve, reject }, () => this.onAdd(newData));
    });
  }


  onRowUpdate = (newData, oldData) =>
    new Promise((resolve, reject) => {
      // Copy current state data to a new array
      const data = [...this.state.data];
      // Get edited row index
      const index = data.indexOf(oldData);
      // replace old data
      data[index] = newData;
      // update state with the new array
      const updatedAt = new Date();
      this.setState({ ...this.state, data, updatedAt, resolve, reject }, () => this.onUpdate(newData, oldData));
    });

  onRowDelete = oldData =>
    new Promise((resolve, reject) => {
      let data = [...this.state.data];
      const index = data.indexOf(oldData);
      data.splice(index, 1);
      const updatedAt = new Date();
      this.setState({ ...this.state, data, updatedAt, resolve, reject });
    });

  render() {
    return (
      <MaterialTable
        icons={tableIcons}
        title={this.state.title}
        columns={this.state.columns}
        data={this.state.data}
        options={this.state.options}
        editable={{
          isEditable: rowData => this.props.editable,
          isDeletable: rowData => this.props.deletable,
          onRowAdd: this.props.insertable? this.onRowAdd: null,
          onRowUpdate: this.onRowUpdate,
          onRowDelete: this.onRowDelete
        }}

        localization={{
          pagination: {
            labelDisplayedRows: '{from}-{to} de {count}',
            labelRowsSelect: 'registros',
            labelRowsPerPage: 'Registros por página:',
            firstAriaLabel: 'Primer página',
            firstTooltip: 'Primer página',
            previousAriaLabel: 'Pâgina anterior',
            previousTooltip: 'Página anterior',
            nextAriaLabel: 'Próxima página',
            nextTooltip: 'Próxima página',
            lastAriaLabel: 'Ultima página',
            lastTooltip: 'Ultima página'
          },
          toolbar: {
            nRowsSelected: '{0} registro(s) seleccionado(s)',
            searchTooltip: 'Buscar',
            searchPlaceholder: 'Buscar'
          },
          header: {
            actions: 'Acciones'
          },
          body: {
            emptyDataSourceMessage: 'No se encontraron registros',
            addTooltip: this.state.insertable ? 'Agregar': null,
            deleteTooltip: this.state.deletable ? 'Eliminar': null,
            editTooltip: 'Editar',
            editRow: {
              deleteText: 'Está seguro que desea eliminar este registro?',
              cancelTooltip: 'Cancelar',
              saveTooltip: 'Grabar'
            },
            filterRow: {
              filterTooltip: 'Filtro'
            }
          }
        }}

      />
    );
  }
}

export default EditTable;
