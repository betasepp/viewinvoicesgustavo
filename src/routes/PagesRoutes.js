import React from 'react'

//errors
const Page404 = React.lazy(() => import('../components/Pages/Page404/Page404'))
const Page423 = React.lazy(() => import('../components/Pages/Page423/Page423'))
const Page500 = React.lazy(() => import('../components/Pages/Page500/Page500'))

//principal
const Dashboard = React.lazy(() => import('../components/Dashboard/Dashboard'))//import('../views/dashboard/Dashboard'))

// operaciones
const ClienteOperNueva = React.lazy(() => import('../components/Pages/Page423/Page423'))// import('./views/Operaciones/Nueva'));



const routes = [
  { path: '/404', name: 'Page 404', element: Page404 },
  { path: '/423', name: 'Page 423', element: Page423 },
  { path: '/500', name: 'Page 500', element: Page500 },

  { path: '/cliente_operaciones_nueva', name: 'Cliente Nueva Operación', element: ClienteOperNueva },

]

export default routes
