import Axios from 'axios'
const STATUS_OK = 200
const { 
  API_FIORITO_URL: host, 
  API_FIORITO_VERSION: version, 
  AUTH_FIORITO_URL: auth_host 
} = window;

export async function WhoAmI() {
  let result = []
  await Axios.get(auth_host + '/login/whoami')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response
      } else {
        result = response
      }
    })
    .catch((error) => {})
  return result
}

export async function Loguing(user) {
  let result = {}
  //console.log("host : ?? " + auth_host)
  await Axios.post(auth_host + '/login', user)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response
      } else {
        result = response
      }
    })
    .catch((error) => {
      throw error
    })

  return result
}
