/* eslint-disable prettier/prettier */
import Axios from 'axios';
import { consoleLog } from '@helpers/log-helper';

const STATUS_OK = 200
const STATUS_CREATED = 201
const STATUS_ACCEPTED = 202

const {
    API_FIORITO_URL: host,
    API_FIORITO_VERSION: version,
} = window

const uriBase = host + version
const uriClientes = uriBase + "/clientes"
const uriDashboard = uriBase + "/dashboard"
export const uriReportsTreeContent = uriBase + "/reports/content/tree"



// ********************
// AUTH
// ********************
export async function WhoAmI() {
    let result = 
        {nombreUsuario: 'Alfonso Martinez', legajoUsuario: '123-6', gruposAzure: ['Administrador', 'BackOffice'], token: 'NKLJHJKLDNDUISHU*DGY&*WGDWYD&GW*UDIB^&GDUBDBIU*IU@YTWGHJBBMN++==='}
    ;
    return result;
}

export async function Ingresar(user) {
    let result = {
        data: {
            success: true,
            payload:
                {nombreUsuario: 'Alfonso Martinez', legajoUsuario: '123-6', gruposAzure: ['Administrador', 'BackOffice']}
        },
        headers: {
            authorization:'NKLJHJKLDNDUISHU*DGY&*WGDWYD&GW*UDIB^&GDUBDBIU*IU@YTWGHJBBMN++==='
        }
    };

    return result;
}

// ********************
// OBTENER CLIENTES
// ********************
export async function ObtenerClientesOrdenado(search) {
    let result = [
        {"cuit":11111111111,"razonSocial":"GARCIA, JUAN 1"},
        {"cuit":22222222222,"razonSocial":"GARCIA, JUAN 2"},
        {"cuit":33333333333,"razonSocial":"GARCIA, JUAN 3"},
        {"cuit":44444444444,"razonSocial":"GARCIA, JUAN 4"},
        {"cuit":55555555555,"razonSocial":"GARCIA, JUAN 5"},
        {"cuit":66666666666,"razonSocial":"GARCIA, JUAN 6"},
        {"cuit":77777777777,"razonSocial":"GARCIA, JUAN 7"},
        {"cuit":88888888888,"razonSocial":"GARCIA, JUAN 8"},
        {"cuit":99999999999,"razonSocial":"GARCIA, JUAN 9"},
        {"cuit":10101010101,"razonSocial":"GARCIA, JUAN 10"},
        {"cuit":12121212121,"razonSocial":"GARCIA, JUAN 12"}
    
    ];
    // await Axios.get(uriClientes + search).then(response => {
    //     if(response.status === STATUS_OK ) {
    //         result = response.data;
    //         if(result) {
    //             result = result.sort((a, b) => (a.razonSocial > b.razonSocial) ? 1 : -1);
    //         }
    //     } else {
    //         consoleLog("ObtenerClientesOrdenado status --> ",search, response.status, " - ", response.statusText);
    //       }
    // }).catch(error => {
    //     consoleLog("ObtenerClientesOrdenado axios error --> ", search, error);
    // });
    return result;
}

export async function ListarClientesPorRazonSocial(nombre) {
    return ObtenerClientesOrdenado("/findByRazonSocial/" + nombre);
}

export async function ObtenerClientesPorCuit(cuit) {
    return ObtenerClientesOrdenado("/findByCuit/" + cuit);
}

export async function ObtenerClientesPorNombre(nombre) {
    return ObtenerClientesOrdenado("/findByName/" + nombre);
}

export async function ObtenerClientesPorRazonSocial(nombre) {
    return ObtenerClientesOrdenado("/findBySocialName/" + nombre);
}

// ********************
// CHEQUES
// ********************
export async function ObtenerValidacionCheque(operacionTipo) {
    let result = 
        {"valorTasa": "20", "tasaMinima": "8", "porcentajeComision": "0.5", "valorComission": "8000", "acciones":["aceptar", "rechazar", "eliminar"]}
    ;
    return result;
}


// ********************
// OPERACIONES
// ********************
export async function BuscarOperaciones(search, tipos, estados) {
    let result = [
        {"cuit":22222222222,"razonSocial":"GARCIA, JUAN 2","acciones":["ver", "editar"],"nro":"1", "fecha":"20/10/2019", "estado":"pendiente de compra", "estadoColor":"3", "cantidadCheques": "4", "montoCheques":"$ 2.549.000"},
        {"cuit":22222222222,"razonSocial":"GARCIA, JUAN 2","acciones":["ver"],"nro":"2", "fecha":"25/11/2020", "estado":"terminada", "estadoColor":"1", "cantidadCheques": "10", "montoCheques":"$ 12.400.000"},
        {"cuit":22222222222,"razonSocial":"GARCIA, JUAN 2","acciones":["ver", "editar"],"nro":"3", "fecha":"25/01/2020", "estado":"comprado formalizado pend cobro", "estadoColor":"1", "cantidadCheques": "8", "montoCheques":"$ 28.750.000"},
    ];
    // await Axios.get(uriBase + "/operaciones/find/" + search + "/" + tipos + "/" + estados).then(response => {
    //     consoleLog("BuscarOperaciones response", JSON.stringify(response));
    //     if(response.status === STATUS_OK ) {
    //         result = response.data;
    //     } else {
    //         consoleLog("BuscarOperaciones status --> ", response.status, " - ", response.statusText);
    //       }
    // }).catch(error => {
    //     consoleLog("BuscarOperaciones axios error --> ", error);
    // });
    return result;
}

export async function BuscarOperacionesPrevias(search, tipos, estados) {
    let result = [
        {"acciones":["ver", "editar"],"nro":"1", "fecha":"20/10/2019", "estado":"pendiente de compra", "estadoColor":"3", "cantidadCheques": "4", "montoCheques":"$ 2.549.000"},
        {"acciones":["ver"],"nro":"2", "fecha":"25/11/2020", "estado":"terminada", "estadoColor":"1", "cantidadCheques": "10", "montoCheques":"$ 12.400.000"},
        {"acciones":["ver", "editar"],"nro":"3", "fecha":"25/01/2020", "estado":"comprado formalizado pend cobro", "estadoColor":"1", "cantidadCheques": "8", "montoCheques":"$ 28.750.000"},
    ];
    // await Axios.get(uriBase + "/operacionesprevias/find/" + search + "/" + tipos + "/" + estados).then(response => {
    //     consoleLog("BuscarOperaciones response", JSON.stringify(response));
    //     if(response.status === STATUS_OK ) {
    //         result = response.data;
    //     } else {
    //         consoleLog("BuscarOperaciones status --> ", response.status, " - ", response.statusText);
    //       }
    // }).catch(error => {
    //     consoleLog("BuscarOperaciones axios error --> ", error);
    // });
    return result;
}

export async function GrabarOperacion(operacion) {
    let result = true;
    // let payload = { operacion };
    // await Axios.post(uriBase + "/operaciones", operacion).then(response => {
    //     // consoleLog("GrabarOperacion response", response);
    //     if(response.status === STATUS_CREATED || response.status === STATUS_ACCEPTED) {
    //         consoleLog("GrabarOperacion", JSON.stringify(operacion));
    //         result = true; 
    //     } else {
    //         consoleLog("GrabarOperacion status --> ", response.status, " - ", response.statusText);
    //       }
    // }).catch(error => {
    //     consoleLog("GrabarOperacion axios error --> ", error);
    // });
    return result;
}

export async function ObtenerOperacionTipos() {
    return ObtenerOrdenado("/operacionTipos");
}

export async function ObtenerOperacionDetalle(operacionTipo) {
    let result = [];
    await Axios.get(uriBase + "/configuracion/findByCodigo/OPERACION_DETALLE").then(response => {
        // consoleLog("ObtenerOperacionDetalle response", response);
        if(response.status === STATUS_OK ) {
            result = JSON.parse(response.data.valor); 
        } else {
            consoleLog("ObtenerOperacionDetalle status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerOperacionDetalle axios error --> ", error);
    });
    if (result) {
        return result[operacionTipo];
    }
    return result;
}

export async function ObtenerOperacionSubtipos(operacionTipo) {
    let result = [];
    await Axios.get(uriBase + "/operacionTipos/findByCodigo/" + operacionTipo).then(response => {
        // consoleLog("ObtenerOperacionSubtipos response", response);
        if(response.status === STATUS_OK ) {
            result = response.data.subtipos;
        } else {
            consoleLog("ObtenerOperacionSubtipos status --> ", response.status, " - ", response.statusText);
        }
    }).catch(error => {
        consoleLog("ObtenerOperacionSubtipos axios error --> ", error);
    });
    return result;
}

export async function ObtenerOperacionEstados() {
    return ObtenerOrdenado("/operacionEstados");
}







// ********************
// LOGS
// ********************
export async function ObtenerLogs()  {
    let result = [];
    await Axios.get(uriBase + "/auditLogs").then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data; 
        } else {
            consoleLog("ObtenerLogs status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerLogs axios error --> ", error);
    });
    return result;
}





// ********************
// REPORTES
// ********************
export async function BuscarOperacionesReporte(tipos, startDate, endDate) {
    let result = [];
    await Axios.get(uriBase + "/operaciones/findReport/" + tipos + "/" + startDate + "/" + endDate).then(response => {
        // consoleLog("BuscarOperaciones response", response);
        if(response.status === STATUS_OK ) {
            response.data.map(item => {
                result.push({'cuit': item.cuit, 'razonSocial': item.razonSocial,'tipo': item.tipodescripcion, 'subtipo': item.subtipodescripcion, 'fechaAlta': item.fechaAlta, 'estado': item.estadodescripcion});
            });
        } else {
            consoleLog("BuscarOperaciones status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("BuscarOperaciones axios error --> ", error);
    });
    return result;
}

export async function ObtenerClientesPorRazonSocialReporte(nombre) {
    let result = [];
    await Axios.get(uriClientes + "/findBySocialName/" + nombre).then(response => {
        // consoleLog("ObtenerClientesPorRazonSocial response", response);
        if(response.status === STATUS_OK ) {
            result = response.data;
            if(result) {
                result = result.sort((a, b) => (a.razonSocial > b.razonSocial) ? 1 : -1);
            }
        } else {
            consoleLog("ObtenerClientesPorRazonSocial status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerClientesPorRazonSocial axios error --> ", error);
    });
    return result;
}





// ********************
// CONFIGURACION
// ********************
export async function ObtenerTablasMenores()  {
    let result = [];
    await Axios.get(uriBase + "/tablasmenores").then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data; 
        } else {
            consoleLog("ObtenerTablasMenores status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerTablasMenores axios error --> ", error);
    });
    return result;
}

export async function ObtenerComponente(componente)  {
    let result = [];
    await Axios.get(uriBase + "/" + componente).then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data; 
        } else {
            consoleLog("ObtenerComponente status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerComponente axios error --> ", error);
    });
    return result;
}

export async function ObtenerSistema()  {
    let result = [];
    await Axios.get(uriBase + "/sistema").then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data; 
        } else {
            consoleLog("ObtenerSistema status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerSistema axios error --> ", error);
    });
    return result;
}




// ********************
// GENERICO
// ********************
export async function ObtenerOrdenado(busqueda) {
    let result = [];
    await Axios.get(uriBase + busqueda).then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data.sort((a, b) => (a.descripcion > b.descripcion) ? 1 : -1);
        } else {
            consoleLog("ObtenerOrdenado status --> ", busqueda, " - ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerOrdenado axios error --> ", busqueda, " - ", error);
    });
    return result;
}


// ********************
// DASHBOARD
// ********************
export async function DashboardClientes() {
    return ObtenerDashboard("/clientes");
}

export async function DashboardOperaciones() {
    return ObtenerDashboard("/operaciones");
}

export async function DashboardOperacionesNuevas() {
    return ObtenerDashboard("/operacionesnuevas");
}

export async function DashboardCantidadNuevas() {
    return ObtenerDashboard("/cantidadoperacionesnuevas");
}

export async function DashboardCantidadBaja() {
    return ObtenerDashboard("/cantidadoperacionesbaja");
}

export async function DashboardCantidadNoCLientes() {
    return ObtenerDashboard("/cantidadnoclientes");
}

export async function DashboardCantidadSiCLientes() {
    return ObtenerDashboard("/cantidadsiclientes");
}


async function ObtenerDashboard(dashboard) {
    let result = [];
    await Axios.get(uriDashboard + dashboard).then(response => {
        if(response.status === STATUS_OK ) {
            result = response.data;
        } else {
            consoleLog("ObtenerDashboard " + dashboard + " status --> ", response.status, " - ", response.statusText);
          }
    }).catch(error => {
        consoleLog("ObtenerDashboard " + dashboard + " axios error --> ", error);
    });
    return result;
}