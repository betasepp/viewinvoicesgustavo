import Axios from 'axios';
import { apiInvoices } from './systemServices';

export const printerInvoiceExport = async (reference) => {
    const {apiUrl} = apiInvoices
     let report ;
      await Axios.get(apiUrl + "/printer/invoicesExport?nameReport=invoice&reference=" + reference)
      .then(
         response => {
            report = response.data;
         }
      ).catch(error => {
        report=null;
      });
     return report;
}

export const printerInvoice = async (reference) => {
   const {apiUrl} = apiInvoices
    let report ;
     await Axios.get(apiUrl + "/printer/invoices?nameReport=invoice&reference=" + reference)
     .then(
        response => {
           report = response.data;
        }
     ).catch(error => {
       report=null;
     });
    return report;
}

export const printerInvoiceDownload = async (reference) =>{
   const {apiUrl} = apiInvoices
   let result = [];
   await Axios.get(apiUrl + "/printer/invoicesDonwload?code=" + reference,{ responseType: 'blob' }).then(response => {
       if (response.status && response.status !== undefined && response.status === STATUS_OK) {
           result = response.data;
       } else {
           result = response;
       }
   }).catch(error => {
       result = error;
   });
   return result;
}