import Axios from 'axios';

export const apiInvoicesOld = {
   port: process.env.REACT_APP_port,
   contextPath: process.env.REACT_APP_contextPath,
   apiUrl: process.env.REACT_APP_port + process.env.REACT_APP_contextPath,
   toMail: process.env.REACT_APP_toMail,
   requestAuth: process.env.REACT_APP_requestAuth,
   requestUser: process.env.REACT_APP_requestAuth_user,
   requestPass: process.env.REACT_APP_requestAuth_pass,
};


const  {
  REACT_APP_PORT: port,
  REACT_APP_CONTEXTPATH:contextPath,
  REACT_APP_TOMAIL: toMail,
  REACT_APP_REQUESTAUTH: requestAuth,
  REACT_APP_REQUESTAUTH_USER  : requestUser,
  REACT_APP_REQUESTAUTH_PASS:requestPass
} = window;

const apiUrl = port + contextPath;

export const apiInvoices = {
  port: port,
  contextPath: contextPath,
  tokenName: contextPath.replace("/","_"),
  apiUrl: apiUrl,
  toMail: toMail,
  requestAuth: requestAuth,
  requestUser: requestUser,
  requestPass:requestPass
};

const STATUS_OK = 200;
//const STATUS_CREATED = 201;
//const STATUS_ACCEPTED = 202;
export const versionFront = {
  version : "1.0.14 (2023-03-18)"
};

export async function WhoAmI() {
  const {apiUrl} = apiInvoices
  let result = [];
  await Axios.get(apiUrl + "/login/whoami").then(response => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
          result = response;
      } else {
          result = response;
      }
  }).catch(error => {
  });
  console.log(result)
  return result;
}

export async function loginAuto() {
  const user = { username: requestUser, password: requestPass }
  let result = {}
  await Axios.post(apiUrl + '/login', user) 
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response
      } else {
        result = response
      }
    })
    .catch((error) => {
      throw error
    })

  return result
}


 export const getCompanyDB = async () => {
     let company = {};
      await Axios.get(apiUrl + "/company/data")
      .then(response => {
        company = response.data;
      }).catch(error => {
        company=null;
      });
     return company;
}

export const versionBack = async () => {
   let version = {};
   console.log(apiUrl)
    await Axios.get(apiUrl + "/company/systemVersion")
    .then(response => {
      version = response.data;
    }).catch(error => {
      version=null;
    });
   return version;
}



